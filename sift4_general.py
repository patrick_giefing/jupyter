import re

# Python port from https://siderite.dev/blog/super-fast-and-accurate-string-distance.html/

# Sift4 - extended version
# online algorithm to compute the distance between two strings in O(n)
# maxOffset is the number of positions to search for matching tokens
# options: the options for the function, allowing for customization of the scope and algorithm:
#         maxDistance: the distance at which the algorithm should stop computing the value and just exit (the strings are too different anyway)
#         tokenizer: a function to transform strings into vectors of tokens
#        tokenMatcher: a function to determine if two tokens are matching (equal)
#        matchingEvaluator: a function to determine the way a token match should be added to the local_cs. For example a fuzzy match could be implemented.
#        localLengthEvaluator: a function to determine the way the local_cs value is added to the lcss. For example longer continuous substrings could be awarded.
#        transpositionCostEvaluator: a function to determine the value of an individual transposition. For example longer transpositions should have a higher cost.
#        transpositionsEvaluator: a function to determine the way the total cost of transpositions affects the final result
# the options can and should be implemented at a class level, but this is the demo algorithm
def sift4(s1, s2, maxOffset, options=None):
    if not options: options = {}
    options['maxDistance'] = None
    options['tokenizer'] = lambda s: list(s) if s else []
    options['tokenMatcher'] = lambda t1, t2: t1 == t2
    options['matchingEvaluator'] = lambda t1, t2: 1
    options['localLengthEvaluator'] = lambda local_cs: local_cs
    options['transpositionCostEvaluator'] = lambda c1, c2: 1
    options['transpositionsEvaluator'] = lambda lcss, trans: lcss - trans

    t1 = options['tokenizer'](s1)
    t2 = options['tokenizer'](s2)

    l1 = len(t1)
    l2 = len(t2)

    if l1 == 0:
        return l2
    if l2 == 0:
        return l1

    c1 = 0  # cursor for string 1
    c2 = 0  # cursor for string 2
    lcss = 0  # largest common subsequence
    local_cs = 0  # local common substring
    trans = 0  # number of transpositions ('ab' vs 'ba')
    offset_arr = []  # offset pair array, for computing the transpositions
    while (c1 < l1) and (c2 < l2):
        if options['tokenMatcher'](t1[c1], t2[c2]):
            local_cs += options['matchingEvaluator'](t1[c1], t2[c2])
            isTrans = False
            # see if current match is a transposition
            i = 0
            while i < len(offset_arr):
                ofs = offset_arr[i]
                if c1 <= ofs['c1'] or c2 <= ofs['c2']:
                    # when two matches cross, the one considered a transposition is the one with the largest difference in offsets
                    isTrans = abs(c2 - c1) >= abs(ofs['c2'] - ofs['c1'])
                    if isTrans:
                        trans += options['transpositionCostEvaluator'](c1, c2)
                    else:
                        if not ofs['trans']:
                            ofs['trans'] = True
                            trans += options['transpositionCostEvaluator'](ofs['c1'], ofs['c2'])
                    break
                else:
                    if c1 > ofs['c2'] and c2 > ofs['c1']:
                        del offset_arr[i]
                    else:
                        i += 1
            offset = {
                'c1': c1,
                'c2': c2,
                'trans': isTrans
            }
            offset_arr.append(offset)
        else:
            lcss += options['localLengthEvaluator'](local_cs)
            local_cs = 0
            if c1 != c2:
                c1 = c2 = min(c1, c2)  # using min allows the computation of transpositions
            # if matching tokens are found, remove 1 from both cursors (they get incremented at the end of the loop)
            # so that we can have only one code block handling matches
            i = 0
            while True:
                if not (i < maxOffset and (c1 + i < l1 or c2 + i < l2)):
                    break
                if (c1 + i < l1) and options['tokenMatcher'](t1[c1 + i], t2[c2]):
                    c1 += i - 1
                    c2 -= 1
                    break
                if (c2 + i < l2) and options['tokenMatcher'](t1[c1], t2[c2 + i]):
                    c1 -= 1
                    c2 += i - 1
                    break
                i += 1
        c1 += 1
        c2 += 1
        if options['maxDistance']:
            temporaryDistance = options['localLengthEvaluator'](max(c1, c2)) - options['transpositionsEvaluator'](lcss,
                                                                                                                  trans)
            if temporaryDistance >= options['maxDistance']:
                return round(temporaryDistance)
        # this covers the case where the last match is on the last token in list, so that it can compute transpositions correctly
        if (c1 >= l1) or (c2 >= l2):
            lcss += options['localLengthEvaluator'](local_cs)
            local_cs = 0
            c1 = c2 = min(c1, c2)
    lcss += options['localLengthEvaluator'](local_cs)
    return round(options['localLengthEvaluator'](max(l1, l2)) - options['transpositionsEvaluator'](lcss, trans))  # add the cost of found transpositions


def extend(obj, props):
    result = {}
    for prop in props:
        if not obj or not obj.hasOwnProperty(prop):
            result[prop] = props[prop]
        else:
            result[prop] = obj[prop]
    return result


# possible values for the options
# tokenizers:
def nGramTokenizer(s, n):  # tokenizer:function(s) { return nGramTokenizer(s,2); }
    result = []
    if not s:
        return result
    for i in range(0, len(s) - n + 1):
        result.append(s.substr(i, n))
    return result


def wordSplitTokenizer(s):  # tokenizer:wordSplitTokenizer
    if not s:
        return []
    return re.split("\s+", s)


def characterFrequencyTokenizer(s):  # tokenizer:characterFrequencyTokenizer (letters only)
    result = []
    for i in range(0, 26):
        val = 0
        if s:
            for j in range(0, len(s)):
                code = s.charCodeAt(j)
                if code == i + 65 or code == i + 97:
                    val += 1
        result.append(val)
    return result


# tokenMatchers:
def sift4TokenMatcher(t1, t2):  # tokenMatcher:sift4TokenMatcher
    similarity = 1 - sift4(t1, t2, 5) / max(t1.length, t2.length)
    return similarity > 0.7


# matchingEvaluators:
def sift4MatchingEvaluator(t1, t2):  # matchingEvaluator:sift4MatchingEvaluator
    similarity = 1 - sift4(t1, t2, 5) / max(t1.length, t2.length)
    return similarity


# localLengthEvaluators:
def rewardLengthEvaluator(l):
    if l < 1:
        return l  # 0 -> 0
    return l - 1 / (l + 1)  # 1 -> 0.5, 2-> 0.66, 9 -> 0.9


def rewardLengthEvaluator2(l):
    return l ** 1.5  # 0 -> 0, 1 -> 1, 2 -> 2.83, 10 -> 31.62


# transpositionCostEvaluators:
def longerTranspositionsAreMoreCostly(c1, c2):
    return abs(c2 - c1) / 9 + 1
